#include <ldap.h>
#include <stdio.h>
#include <stdlib.h>

#include "ldap-settings.h"

int
main ()
{
  LDAP  *ld;
  char ldap_uri[100];
  int rc;
  int msgid;
  int err;
  struct berval passwd;
  LDAPMessage *result;
  LDAPMessage *msg;
  LDAPControl **ctrls = NULL;
  char *matched = NULL;
  char *info = NULL;
  char **refs = NULL;


  const int protocol = LDAP_VERSION3;

  sprintf (ldap_uri, "ldap://%s/??base", LDAP_HOST);

  printf("** LDAP URI: %s\n", ldap_uri);
  rc = ldap_initialize (&ld, ldap_uri);
  if (rc != LDAP_SUCCESS) {
    fprintf (stderr, "Could not create LDAP session handle for URI=%s (%d): %s\n",
        ldap_uri, rc, ldap_err2string(rc));
    exit (EXIT_FAILURE);
  }

  rc = ldap_set_option (ld, LDAP_OPT_PROTOCOL_VERSION, &protocol);
  if (rc != LDAP_OPT_SUCCESS) {
    fprintf (stderr, "Could not set LDAP_OPT_PROTOCOL_VERSION %d\n",
        protocol);
    exit (EXIT_FAILURE);
  }

  passwd.bv_val = ber_strdup (LDAP_PASSWORD);
  passwd.bv_len = strlen (passwd.bv_val);

  printf("** BIND DN: %s\n", LDAP_AUTH_DN);
  printf("** PASSWD: %s\n", passwd.bv_val);
  rc = ldap_sasl_bind (ld, LDAP_AUTH_DN, LDAP_SASL_SIMPLE, &passwd, NULL,
      NULL, &msgid);
  if (msgid == -1) {
    fprintf (stderr, "ldap_sasl_bind (%d): %s\n", rc,
        ldap_err2string(rc));
    exit (EXIT_FAILURE);
  }

  rc = ldap_result (ld, msgid, LDAP_MSG_ALL, NULL, &result);
  if (rc <= 0) {
    fprintf (stderr, "ldap_result (%d): %s\n", rc, ldap_err2string(rc));
    exit (EXIT_FAILURE);
  }

  if (result) {
    rc = ldap_parse_result (ld, result, &err, &matched, &info, &refs, &ctrls, 1);
    if (rc != LDAP_SUCCESS) {
      fprintf (stderr, "ldap_parse_result matched: %s, info: %s(%d): %s\n",
          matched, info, rc, ldap_err2string(rc));
      exit (EXIT_FAILURE);
    }
  }

  if (err != LDAP_SUCCESS) {
    fprintf (stderr, "ldap_parse_result err: (%d): %s\n",
        err, ldap_err2string(err));
    exit (EXIT_FAILURE);
  }

  rc = ldap_search_ext (ld, LDAP_ROOT_DN, LDAP_SCOPE_SUBTREE, LDAP_QUERY, NULL,
      0, NULL, NULL, NULL, -1, &msgid);
  if (rc != LDAP_SUCCESS) {
    fprintf (stderr, "ldap_search_ext (%d): %s\n", rc, ldap_err2string(rc));
    exit (EXIT_FAILURE);
  }

  while ((rc = ldap_result (ld, LDAP_RES_ANY, LDAP_MSG_ONE, NULL, &result)) > 0) {
    for (msg = ldap_first_message (ld, result); msg; msg = ldap_next_message(ld, msg)) {
      switch (ldap_msgtype (msg)) {
        case LDAP_RES_SEARCH_ENTRY: {
          char *dn = ldap_get_dn (ld, msg);
          printf ("Found entry: %s\n", dn);
          ldap_memfree (dn);
          break;
        }
        case LDAP_RES_SEARCH_RESULT: {
          goto done;
        }
        default:
          printf ("msgtype %d\n", ldap_msgtype (msg));
          break;
      }
    }
    ldap_msgfree (result);
  }

done:
  return EXIT_SUCCESS;
}

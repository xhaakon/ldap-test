#include <ldap.h>
#include <stdio.h>

#include "ldap-settings.h"

int main(int argc, char **argv) {
  LDAP *ldap;
  LDAPMessage *result;
  LDAPMessage *entry;
  int ldap_error;

  const int debug_level = 0xFFFF;
  const int protocol_version = LDAP_VERSION3;

  int debug = 7;
  if (ber_set_option (NULL, LBER_OPT_DEBUG_LEVEL, &debug) != LBER_OPT_SUCCESS) {
    fprintf (stderr, "Could not set LBER_OPT_DEBUG_LEVEL %d\n", debug);
  }
  if (ldap_set_option( NULL, LDAP_OPT_DEBUG_LEVEL, &debug) != LDAP_OPT_SUCCESS) {
    fprintf (stderr, "Could not set LDAP_OPT_DEBUG_LEVEL %d\n", debug);
  }

  ldap = ldap_init (LDAP_HOST, 389);
  ldap_set_option (ldap, LDAP_OPT_DEBUG_LEVEL, &debug_level);
  ldap_error = ldap_set_option (ldap, LDAP_OPT_PROTOCOL_VERSION, &protocol_version);
  if (ldap_error != LDAP_SUCCESS) {
    fprintf (stderr, "failed to set protocol version to LDAPv3\n");
    return 1;
  }

  ldap_error = ldap_simple_bind_s (ldap, LDAP_AUTH_DN, LDAP_PASSWORD);
  if (ldap_error != LDAP_SUCCESS) {
    fprintf (stderr, "ldap_simple_bind_s failed: %s\n", ldap_err2string (ldap_error));
    return 1;
  }

  ldap_error = ldap_search_ext_s (ldap, LDAP_ROOT_DN, LDAP_SCOPE_SUBTREE, LDAP_QUERY,
      NULL, 0, NULL, NULL, NULL, 100, &result);
  if (ldap_error != LDAP_SUCCESS) {
    fprintf (stderr, "ldap_search_ext failed: %s\n", ldap_err2string (ldap_error));
    return 1;
  }

  switch (ldap_msgtype (result)) {
  case LDAP_RES_SEARCH_ENTRY:
    entry = ldap_first_entry (ldap, result);
    while (entry) {
      char *dn = ldap_get_dn (ldap, entry);
      printf ("Found entry: %s\n", dn);
      ldap_memfree (dn);

      entry = ldap_next_entry (ldap, entry);
    }
    break;
  case LDAP_RES_SEARCH_RESULT: {
    char *ldap_error_msg = NULL;
    ldap_parse_result (ldap, result, &ldap_error,
        NULL, &ldap_error_msg, NULL, NULL, 0);
    if (ldap_error != LDAP_SUCCESS) {
      fprintf (stderr, "ldap_parse_result: %s, %s\n",
          ldap_err2string (ldap_error), ldap_error_msg);
      if (ldap_error_msg) {
        ldap_memfree (ldap_error_msg);
      }
    } else {
      printf ("No results found\n");
    }
    break;
  }
  default:
    break;
  }
}
